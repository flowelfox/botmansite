from asgiref.sync import async_to_sync
from avatar.models import Avatar
from avatar.signals import avatar_updated
from avatar.views import _get_avatars
from channels.layers import get_channel_layer
from django.contrib.auth import authenticate, logout, login
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponseRedirect, JsonResponse
from django.shortcuts import render
from django.utils.translation import gettext_lazy as _
from django.views import View
from django.views.generic import TemplateView

from website import tasks
from website.forms import CustomUserChangeForm


class NamedPageMixin:
    page_name = _("Unnamed")

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['page_name'] = getattr(self, 'page_name', None)
        context["page_full_name"] = getattr(self, 'page_full_name', None)
        return context


class Dashboard(NamedPageMixin, LoginRequiredMixin, TemplateView):
    login_url = '/login'
    template_name = 'dashboard.html'
    page_name = _("Dashboard")


class Chats(NamedPageMixin, LoginRequiredMixin, TemplateView):
    login_url = '/login'
    template_name = 'chats.html'
    page_name = _("Chats")


class Distribution(NamedPageMixin, LoginRequiredMixin, TemplateView):
    login_url = '/login'
    template_name = 'distribution.html'
    page_name = _("Distribution")


class Control(NamedPageMixin, LoginRequiredMixin, TemplateView):
    login_url = '/control'
    template_name = 'control.html'
    page_name = _("Control")


class Users(NamedPageMixin, LoginRequiredMixin, TemplateView):
    login_url = '/login'
    template_name = 'users.html'
    page_name = _("Users")


class Profile(NamedPageMixin, LoginRequiredMixin, TemplateView):
    login_url = '/profile'
    template_name = 'profile.html'
    page_full_name = _("Profile")

    def post(self, request, *args, **kwargs):
        avatar, avatars = _get_avatars(request.user)

        change_user_form = CustomUserChangeForm(request.POST or None,
                                                request.FILES or None,
                                                user=request.user,
                                                instance=request.user)

        if change_user_form.is_valid():
            if 'avatar' in request.FILES:
                avatar = Avatar(user=request.user, primary=True)
                image_file = request.FILES['avatar']
                avatar.avatar.save(image_file.name, image_file)
                avatar.save()
                avatar_updated.send(sender=Avatar, user=request.user, avatar=avatar)
            change_user_form.save()

        context = self.get_context_data()
        return render(request, self.template_name, context)


class Notifications(NamedPageMixin, LoginRequiredMixin, TemplateView):
    login_url = '/login'
    template_name = 'notifications.html'
    page_name = _("Notifications")


class Login(View):
    template_name = 'login.html'

    def get(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            return HttpResponseRedirect(request.GET.get("next", '/dashboard'))
        else:
            return render(request, self.template_name, {"message": ""})

    def post(self, request, *args, **kwargs):
        username = request.POST.get("login", None)
        password = request.POST.get("password", None)
        next = request.POST.get("next", "")
        user = authenticate(username=username, password=password)
        if user is not None:
            login(request, user)
            if len(user.bots.all()):
                request.session['selected_bot'] = user.bots.all()[0].id
            else:
                request.session['selected_bot'] = 0
            return HttpResponseRedirect(next)
        return render(request, self.template_name, {"message": "Wrong username or password"})


def logout_view(request):
    logout(request)
    return HttpResponseRedirect('/login')


@login_required
def change_bot(request, *args, **kwargs):
    bot_id = request.GET.get('bot', None)
    # next = request.GET.get('next', '/dashboard')
    try:
        bot_id = int(bot_id)
    except ValueError:
        bot_id = None

    if bot_id is None:
        return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))

    # TODO: Optimize query
    user_bots = request.user.bots.all()
    selected_bot = None
    for bot in user_bots:
        if bot_id == bot.id:
            selected_bot = bot
            break
    if selected_bot:
        request.session['selected_bot'] = int(bot_id)
    return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))


@login_required
def start_bot_view(request, *args, **kwargs):
    bot = request.user.get_selected_bot(request.session)
    if bot:
        tasks.start_bot.delay(request.user.id, bot.get_domain(), bot.token)
    else:
        cl = get_channel_layer()
        async_to_sync(cl.group_send)(f"user-{request.user.id}-notification", {'type': 'receive', 'notification': {'type': 'warning', 'text': "Bot not selected"}})

    return JsonResponse({})


@login_required
def stop_bot_view(request, *args, **kwargs):
    bot = request.user.get_selected_bot(request.session)
    tasks.stop_bot.delay(request.user.id, bot.get_domain(), bot.token)
    return JsonResponse({})


@login_required
def restart_bot_view(request, *args, **kwargs):
    bot = request.user.get_selected_bot(request.session)
    tasks.restart_bot.delay(request.user.id, bot.get_domain(), bot.token)
    return JsonResponse({})


@login_required
def start_distribution_view(request, *args, **kwargs):
    if request.method:
        bot = request.user.get_selected_bot(request.session)

        text = request.POST.get("text", None)
        when = None
        web_preview = request.POST.get("web-preview", False) == 'on'
        notification = request.POST.get("notification", False) == 'on'

        cl = get_channel_layer()
        if bot:
            if text:
                tasks.bot_distribution.delay(request.user.id, bot.get_domain(), bot.token, text, when, web_preview, notification)
                return JsonResponse({"success": True})
            else:
                async_to_sync(cl.group_send)(f"user-{request.user.id}-notification", {'type': 'receive', 'notification': {'type': 'warning', 'text': "Please enter a message"}})

        else:
            async_to_sync(cl.group_send)(f"user-{request.user.id}-notification", {'type': 'receive', 'notification': {'type': 'warning', 'text': "Bot not selected"}})
        return JsonResponse({"success": False})


@login_required
def bot_status_view(request, *args, **kwargs):
    bot = request.user.get_selected_bot(request.session)

    if bot:
        tasks.get_bot_status.delay(request.user.id, bot.get_domain(), bot.token)
    else:
        cl = get_channel_layer()
        async_to_sync(cl.group_send)(f"user-{request.user.id}-control", {'type': 'receive', 'service': 'bot', "status": False})

    return JsonResponse({})


@login_required
def api_status_view(request, *args, **kwargs):
    bot = request.user.get_selected_bot(request.session)

    if bot:
        tasks.get_api_status.delay(request.user.id, bot.get_domain(), bot.token)
    else:
        cl = get_channel_layer()
        async_to_sync(cl.group_send)(f"user-{request.user.id}-control", {'type': 'receive', 'service': 'api', "status": False})

    return JsonResponse({})


@login_required
def dashboard_data(request, *args, **kwargs):
    bot = request.user.get_selected_bot(request.session)

    if bot:
        tasks.get_uptime.delay(request.user.id, bot.get_domain(), bot.token, 'dashboard')
        tasks.get_start_date.delay(request.user.id, bot.get_domain(), bot.token, 'dashboard')
        tasks.get_stats.delay(request.user.id, bot.get_domain(), bot.token, 'dashboard')
        tasks.get_users.delay(request.user.id, bot.get_domain(), bot.token, 'dashboard')

    else:
        cl = get_channel_layer()
        async_to_sync(cl.group_send)(f"user-{request.user.id}-dashboard", {'type': 'receive', 'task': None, 'data': False})

    return JsonResponse({})


@login_required
def users_data(request, *args, **kwargs):
    bot = request.user.get_selected_bot(request.session)

    if bot:
        tasks.get_stats.delay(request.user.id, bot.get_domain(), bot.token, 'users')
        tasks.get_users.delay(request.user.id, bot.get_domain(), bot.token, 'users')

    else:
        cl = get_channel_layer()
        async_to_sync(cl.group_send)(f"user-{request.user.id}-users", {'type': 'receive', 'task': None, 'data': False})

    return JsonResponse({})
