from django import template

register = template.Library()


@register.filter
def get(h, args_string):
    args = args_string.split(':')
    if len(args) == 2:
        key, default = args
    else:
        key, default = (args[0], None)
    return h.get(key, default)
