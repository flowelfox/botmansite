from django import template

register = template.Library()


@register.simple_tag(takes_context=True)
def selected_bot(context):
    selected_bot_id = context.request.session.get('selected_bot', 0)
    selected_bot_name = "Not selected"
    for bot in context.request.user.bots.all():
        if bot.id == selected_bot_id:
            selected_bot_name = bot.name
    return selected_bot_name
