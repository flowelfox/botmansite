from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as DjangoUserAdmin

from .forms import CustomUserCreationForm, CustomUserChangeForm
from .models import User, Bot


class BotAdmin(admin.ModelAdmin):
    model = Bot


class UserAdmin(DjangoUserAdmin):
    add_form = CustomUserCreationForm
    form = CustomUserChangeForm
    model = User
    list_display = ['email', 'username']
    exclude = ("users",)


admin.site.register(User, UserAdmin)
admin.site.register(Bot, BotAdmin)
