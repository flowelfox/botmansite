import json

from asgiref.sync import async_to_sync
from channels.generic.websocket import WebsocketConsumer


class NotificationConsumer(WebsocketConsumer):

    def connect(self):
        async_to_sync(self.channel_layer.group_add)(f"user-{self.scope['user'].id}-notification", self.channel_name)
        self.accept()

    def disconnect(self, close_code):
        async_to_sync(self.channel_layer.group_discard)(f"user-{self.scope['user'].id}-notification", self.channel_name)

    def receive(self, text_data=None, bytes_data=None):
        if isinstance(text_data, str):
            data = json.loads(text_data)
        else:
            data = text_data
        self.send(text_data=json.dumps(data))


class ControlConsumer(WebsocketConsumer):

    def connect(self):
        async_to_sync(self.channel_layer.group_add)(f"user-{self.scope['user'].id}-control", self.channel_name)
        self.accept()

    def disconnect(self, close_code):
        async_to_sync(self.channel_layer.group_discard)(f"user-{self.scope['user'].id}-control", self.channel_name)

    def receive(self, text_data=None, bytes_data=None):
        if isinstance(text_data, str):
            data = json.loads(text_data)
        else:
            data = text_data
        self.send(text_data=json.dumps(data))


class UsersConsumer(WebsocketConsumer):

    def connect(self):
        async_to_sync(self.channel_layer.group_add)(f"user-{self.scope['user'].id}-users", self.channel_name)
        self.accept()

    def disconnect(self, close_code):
        async_to_sync(self.channel_layer.group_discard)(f"user-{self.scope['user'].id}-users", self.channel_name)

    def receive(self, text_data=None, bytes_data=None):
        if isinstance(text_data, str):
            data = json.loads(text_data)
        else:
            data = text_data
        self.send(text_data=json.dumps(data))


class DashboardConsumer(WebsocketConsumer):

    def connect(self):
        async_to_sync(self.channel_layer.group_add)(f"user-{self.scope['user'].id}-dashboard", self.channel_name)
        self.accept()

    def disconnect(self, close_code):
        async_to_sync(self.channel_layer.group_discard)(f"user-{self.scope['user'].id}-dashboard", self.channel_name)

    def receive(self, text_data=None, bytes_data=None):
        if isinstance(text_data, str):
            data = json.loads(text_data)
        else:
            data = text_data
        self.send(text_data=json.dumps(data))