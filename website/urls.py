from django.conf.urls import url
from django.urls import path
from django.views.generic import RedirectView

from website import views

urlpatterns = [
    path('dashboard', views.Dashboard.as_view(), name='dashboard'),
    path('dashboard/data', views.dashboard_data, name='dashboard_data'),
    path('chats', views.Chats.as_view(), name='chats'),
    path('distribution', views.Distribution.as_view(), name='distribution'),
    path('distribution/start', views.start_distribution_view, name='start_distribution'),
    path('users', views.Users.as_view(), name='users'),
    path('users/data', views.users_data, name='users_data'),
    path('notifications', views.Notifications.as_view(), name='notifications'),
    path('control', views.Control.as_view(), name='control'),
    path('control/start_bot', views.start_bot_view, name='start_bot'),
    path('control/stop_bot', views.stop_bot_view, name='stop_bot'),
    path('control/restart_bot', views.restart_bot_view, name='restart_bot'),
    path('control/bot_status', views.bot_status_view, name='bot_status'),
    path('control/api_status', views.api_status_view, name='api_status'),
    path('profile', views.Profile.as_view(), name='profile'),
    path('change_bot', views.change_bot, name='change_bot'),
    path('login', views.Login.as_view(), name='login'),
    path('logout', views.logout_view, name='logout'),
    url(r'^.*$', RedirectView.as_view(url='/dashboard', permanent=False), name='dashboard')
]