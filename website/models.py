import binascii
import hashlib
import json
import os

import requests
from avatar.models import AvatarField
from django.contrib.auth.models import AbstractUser
from django.core.files.storage import get_storage_class
from django.db import models
from django.utils.encoding import force_bytes
from django.utils.translation import ugettext as _
from avatar.conf import settings


def bot_avatar_path_handler(instance=None, filename=None, size=None, ext=None):
    tmppath = [settings.AVATAR_STORAGE_DIR]
    tmppath.append(instance.name)
    if not filename:
        # Filename already stored in database
        filename = instance.avatar.name
        if ext and settings.AVATAR_HASH_FILENAMES:
            # An extension was provided, probably because the thumbnail
            # is in a different format than the file. Use it. Because it's
            # only enabled if AVATAR_HASH_FILENAMES is true, we can trust
            # it won't conflict with another filename
            (root, oldext) = os.path.splitext(filename)
            filename = root + "." + ext
    else:
        # File doesn't exist yet
        if settings.AVATAR_HASH_FILENAMES:
            (root, ext) = os.path.splitext(filename)
            if settings.AVATAR_RANDOMIZE_HASHES:
                filename = binascii.hexlify(os.urandom(16)).decode('ascii')
            else:
                filename = hashlib.md5(force_bytes(filename)).hexdigest()
            filename = filename + ext
    if size:
        tmppath.extend(['resized', str(size)])
    tmppath.append(os.path.basename(filename))
    return os.path.join(*tmppath)


avatar_storage = get_storage_class(settings.AVATAR_STORAGE)()


class BotAvatarField(models.ImageField):

    def __init__(self, *args, **kwargs):
        super(BotAvatarField, self).__init__(*args, **kwargs)

        self.max_length = 1024
        self.upload_to = bot_avatar_path_handler
        self.storage = avatar_storage
        self.blank = True

    def deconstruct(self):
        name, path, args, kwargs = super(models.ImageField, self).deconstruct()
        return name, path, (), {}


class Bot(models.Model):
    name = models.CharField(max_length=200)
    username = models.CharField(max_length=32)
    token = models.CharField(max_length=128)
    domain = models.URLField(verbose_name="Api url")
    create_date = models.DateTimeField(auto_now=True)
    avatar = BotAvatarField(verbose_name=_("avatar"))
    users = models.ManyToManyField("User", blank=True, related_name='bots')

    def __str__(self):
        return self.name

    def _headers(self):
        return {"X-API-KEY": self.token}

    def get_domain(self):
        if self.domain.endswith('/'):
            return self.domain
        else:
            return self.domain + '/'

    def get_api_status(self):
        endpoint = self.get_domain() + 'status'
        try:
            # TODO: verify SSL
            # res = requests.get(endpoint, verify=path.join(PROJECT_PATH, 'botmanCA.pem'))
            res = requests.get(endpoint, verify=False)
            if res.status_code == 200:
                content = json.loads(res.content.decode('utf-8'))
                return content.get('status')
            else:
                return False
        except requests.exceptions.ConnectionError:
            return False

    def get_bot_status(self):
        endpoint = self.get_domain() + 'bot/status'
        try:
            # TODO: verify SSL
            # res = requests.get(endpoint, verify=path.join(PROJECT_PATH, 'botmanCA.pem'))
            res = requests.get(endpoint, headers=self._headers(), verify=False)
            if res.status_code == 200:
                content = json.loads(res.content.decode('utf-8'))
                return content.get('status')
            else:
                return False
        except requests.exceptions.ConnectionError:
            return False


class User(AbstractUser):

    def get_selected_bot(self, session):
        selected_bot = session.get('selected_bot')

        if selected_bot is None:
            user_bots = self.bots.all()
            if user_bots:
                bot = user_bots[0]
                session['selected_bot'] = bot.id
            else:
                bot = None
        else:
            try:
                bot = Bot.objects.get(pk=selected_bot)
            except Bot.DoesNotExist:
                bot = None

        return bot
