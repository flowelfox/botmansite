import os

from avatar.models import Avatar
from django import forms
from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from avatar.conf import settings as avatar_settings
from django.template.defaultfilters import filesizeformat
from django.utils.translation import ugettext_lazy as _

from .models import User


class CustomUserCreationForm(UserCreationForm):
    class Meta(UserCreationForm):
        model = User
        fields = ('username', 'email')


class CustomUserChangeForm(UserChangeForm):

    class Meta(UserChangeForm):
        model = User
        fields = ('email', 'first_name', 'last_name')

    avatar = forms.ImageField(label=_("avatar"), required=False)

    def __init__(self, *args, **kwargs):
        if 'user' in kwargs:
            self.user = kwargs.pop('user')
        if 'instance' in kwargs and isinstance(kwargs['instance'], User):
            self.user = kwargs['instance']

        super(CustomUserChangeForm, self).__init__(*args, **kwargs)

    def clean_avatar(self):
        data = self.cleaned_data['avatar']

        if avatar_settings.AVATAR_ALLOWED_FILE_EXTS:
            root, ext = os.path.splitext(data.name.lower())
            if ext not in avatar_settings.AVATAR_ALLOWED_FILE_EXTS:
                valid_exts = ", ".join(avatar_settings.AVATAR_ALLOWED_FILE_EXTS)
                error = _("%(ext)s is an invalid file extension. "
                          "Authorized extensions are : %(valid_exts_list)s")
                raise forms.ValidationError(error %
                                            {'ext': ext,
                                             'valid_exts_list': valid_exts})

        if data and data.size > avatar_settings.AVATAR_MAX_SIZE:
            error = _("Your file is too big (%(size)s), "
                      "the maximum allowed size is %(max_valid_size)s")
            raise forms.ValidationError(error % {
                'size': filesizeformat(data.size),
                'max_valid_size': filesizeformat(avatar_settings.AVATAR_MAX_SIZE)
            })

        count = Avatar.objects.filter(user=self.user).count()
        if 1 < avatar_settings.AVATAR_MAX_AVATARS_PER_USER <= count:
            error = _("You already have %(nb_avatars)d avatars, "
                      "and the maximum allowed is %(nb_max_avatars)d.")
            raise forms.ValidationError(error % {
                'nb_avatars': count,
                'nb_max_avatars': avatar_settings.AVATAR_MAX_AVATARS_PER_USER,
            })
        return
