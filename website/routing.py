from django.urls import path

from . import consumers

websocket_urlpatterns = [
    path(r'ws/notification', consumers.NotificationConsumer),
    path(r'ws/control', consumers.ControlConsumer),
    path(r'ws/users', consumers.UsersConsumer),
    path(r'ws/dashboard', consumers.DashboardConsumer),

]