function initChart(users_per_day, id) {
    $("#" + id + "-container > div").empty();
    var old_array = Object.keys(users_per_day);
    var new_array = old_array.map(function (element) {
        var temp = new Date(parseInt(element));
        if (temp.getMilliseconds() != 0) {
            element = temp.toLocaleString("ru-RU");
        } else {
            element = temp.toLocaleDateString("ru-RU");
        }
        return element;
    });
    var ctx = document.getElementById(id).getContext('2d');
    var myChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: new_array,
            datasets: [{
                backgroundColor: "#43a047",
                label: "Users",
                data: Object.values(users_per_day),
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        min: 0, // it is for ignoring negative step.
                        beginAtZero: true,
                        callback: function (value, index, values) {
                            if (Math.floor(value) === value) {
                                return value;
                            }
                        }
                    }
                }]
            }
        }
    });
}

function initUsersTable(users) {
    let users_table = $("#users-table");

    users.sort((a, b) => (Date.parse(a.join_date) < Date.parse(b.join_date)) ? 1 : -1);

    console.log(users);
    users_table.empty();
    users.slice(0, 5).forEach(function (user) {
        let join_date = new Date(Date.parse(user.join_date));
        let tr = $("<tr></tr>");
        tr.append("<td>" + (user.chat_id == null ? " - " : user.chat_id) + "</td>");
        tr.append("<td>" + (user.first_name == null ? " - " : user.first_name) + " " + (user.last_name == null ? "" : user.last_name) + "</td>");
        tr.append("<td>" + (user.username == null ? " - " : "<a href=\"https://t.me/" + user.username + "\">@" + user.username + "</a>") + "</td>");
        tr.append("<td>" + (user.came_from == null ? " - " : user.came_from) + "</td>");
        tr.append("<td>" + (user.join_date == null ? " - " : join_date.toLocaleString("ru-RU", {year: "numeric", month: "2-digit", day: "2-digit", hour: '2-digit', minute:'2-digit'})) + " UTC" + "</td>");
        users_table.append(tr)
    });

}

function countUpFromTime(alreadyPassed) {
    let countFrom = new Date().getTime() - alreadyPassed * 1000;

    let now = new Date().getTime();

    // Find the distance between now an the count down date
    let distance = now - countFrom;

    // Time calculations for days, hours, minutes and seconds
    let days = Math.floor(distance / (1000 * 60 * 60 * 24));
    let hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
    let minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
    let seconds = Math.floor((distance % (1000 * 60)) / 1000);

    // Output the result in an element with id="demo"
    document.getElementById("uptime").innerHTML = (days ? days + "<small>d</small> " : "") + pad(hours, 2) + "<small>h</small> " + pad(minutes, 2) + "<small>m</small> " + pad(seconds, 2) + "<small>s</small> ";

    clearTimeout(countUpFromTime.interval);
    if (alreadyPassed > 0) {
        countUpFromTime.interval = setTimeout(function () {
            countUpFromTime(alreadyPassed + 1);
        }, 1000);
    }
}


var transparent = true;
var UsersAlltimeChart = undefined;
var UsersMonthChart = undefined;
var UsersWeekChart = undefined;
var UsersDayChart = undefined;
var users = [];
var users_per_day = {};
var users_per_minute_unordered = {};

$(document).ready(function () {

    // $('body').bootstrapMaterialDesign();
    // window_width = $(window).width();

    // //    Activate bootstrap-select
    // if ($(".selectpicker").length != 0) {
    //     $(".selectpicker").selectpicker();
    // }
    //
    // //  Activate the tooltips
    // $('[rel="tooltip"]').tooltip();
    //
    // $('.form-control').on("focus", function () {
    //     $(this).parent('.input-group').addClass("input-group-focus");
    // }).on("blur", function () {
    //     $(this).parent(".input-group").removeClass("input-group-focus");
    // });
    //
    // // remove class has-error for checkbox validation
    // $('input[type="checkbox"][required="true"], input[type="radio"][required="true"]').on('click', function () {
    //     if ($(this).hasClass('error')) {
    //         $(this).closest('div').removeClass('has-error');
    //     }
    // });

// Redraw charts when changing dashboard tabs
    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
            if (e.target.id === "users-alltime-tab" && typeof UsersAlltimeChart !== 'undefined') {
                UsersAlltimeChart.update();
            } else if (e.target.id === "users-alltime-tab" && typeof UsersMonthChart == 'undefined') {
                UsersAlltimeChart = initChart(users_per_day, "users-alltime-chart");
            }

            if (e.target.id === "users-month-tab" && typeof UsersMonthChart !== 'undefined') {
                UsersMonthChart.update();
            } else if (e.target.id === "users-month-tab" && typeof UsersMonthChart == 'undefined') {
                let today = new Date();
                today.setDate(today.getDate() - 30);
                let filtered_users_per_day = Object.keys(users_per_day)
                    .filter(key => key > today.getTime())
                    .reduce((obj, key) => {
                        return {
                            ...obj,
                            [key]: users_per_day[key]
                        };
                    }, {});

                initChart(filtered_users_per_day, "users-month-chart");

            }

            if (e.target.id === "users-week-tab" && typeof UsersWeekChart !== 'undefined') {
                UsersWeekChart.update();
            } else if (e.target.id === "users-week-tab" && typeof UsersWeekChart == 'undefined') {
                let today = new Date();
                today.setDate(today.getDate() - 7);
                let filtered_users_per_day = Object.keys(users_per_day)
                    .filter(key => key > today.getTime())
                    .reduce((obj, key) => {
                        return {
                            ...obj,
                            [key]: users_per_day[key]
                        };
                    }, {});

                UsersWeekChart = initChart(filtered_users_per_day, "users-week-chart");
            }

            if (e.target.id === "users-day-tab" && typeof UsersDayChart !== 'undefined') {
                UsersDayChart.update();
            } else if (e.target.id === "users-day-tab" && typeof UsersDayChart == 'undefined') {
                let today = new Date();
                today.setDate(today.getDate() - 1);
                let filtered_users_per_day = Object.keys(users_per_minute_unordered)
                    .filter(key => key > today.getTime())
                    .reduce((obj, key) => {
                        return {
                            ...obj,
                            [key]: users_per_minute_unordered[key]
                        };
                    }, {});

                UsersDayChart = initChart(filtered_users_per_day, "users-day-chart");
            }
        }
    );

// Redraw charts when resizing window
    $(window).resize(function () {
        if (typeof UsersAlltimeChart !== 'undefined') {
            UsersAlltimeChart.update();
        }
        if (typeof UsersMonthChart !== 'undefined') {
            UsersMonthChart.update();
        }
    });

// Redraw charts when toggling sidebar
    $("#toggle-sidebar").click(function () {

        if (typeof UsersAlltimeChart !== 'undefined') {
            setTimeout(function () {
                UsersAlltimeChart.update();
            }, 500);
        }
        if (typeof UsersMonthChart !== 'undefined') {
            setTimeout(function () {
                UsersMonthChart.update();
            }, 500);
        }
    });


});


md = {
    initFormExtendedDatetimepickers: function () {
        $('.datetimepicker').datetimepicker({
            icons: {
                time: "fa fa-clock-o",
                date: "fa fa-calendar",
                up: "fa fa-chevron-up",
                down: "fa fa-chevron-down",
                previous: 'fa fa-chevron-left',
                next: 'fa fa-chevron-right',
                today: 'fa fa-screenshot',
                clear: 'fa fa-trash',
                close: 'fa fa-remove'
            }
        });

        $('.datepicker').datetimepicker({
            format: 'MM/DD/YYYY',
            icons: {
                time: "fa fa-clock-o",
                date: "fa fa-calendar",
                up: "fa fa-chevron-up",
                down: "fa fa-chevron-down",
                previous: 'fa fa-chevron-left',
                next: 'fa fa-chevron-right',
                today: 'fa fa-screenshot',
                clear: 'fa fa-trash',
                close: 'fa fa-remove'
            }
        });

        $('.timepicker').datetimepicker({
            //          format: 'H:mm',    // use this format if you want the 24hours timepicker
            format: 'h:mm A', //use this format if you want the 12hours timpiecker with AM/PM toggle
            icons: {
                time: "fa fa-clock-o",
                date: "fa fa-calendar",
                up: "fa fa-chevron-up",
                down: "fa fa-chevron-down",
                previous: 'fa fa-chevron-left',
                next: 'fa fa-chevron-right',
                today: 'fa fa-screenshot',
                clear: 'fa fa-trash',
                close: 'fa fa-remove'

            }
        });
    },
    initSliders: function () {
        // Sliders for demo purpose
        var slider = document.getElementById('sliderRegular');

        noUiSlider.create(slider, {
            start: 40,
            connect: [true, false],
            range: {
                min: 0,
                max: 100
            }
        });

        var slider2 = document.getElementById('sliderDouble');

        noUiSlider.create(slider2, {
            start: [20, 60],
            connect: true,
            range: {
                min: 0,
                max: 100
            }
        });
    },
    initUsersChart: function (labels, series, selector) {
        {
            /* ----------==========     Users all time chat initialization    ==========---------- */

            var dataUsersChart = {
                labels: labels,
                series: [
                    series
                ]
            };
            var optionsUsersChart = {
                axisX: {
                    showGrid: false,
                },
                axisY: {
                    onlyInteger: true,
                },
                low: 0,
                high: Math.round((Math.max(...series) + 1) * 1.5),
                chartPadding: {
                    top: 0,
                    right: 5,
                    bottom: 0,
                    left: 0
                },
            };
            var responsiveOptions = [
                ['screen and (max-width: 640px)', {
                    seriesBarDistance: 5,
                    axisX: {
                        labelInterpolationFnc: function (value) {
                            return value[0];
                        }
                    }
                }]
            ];
            // var UsersChart = Chartist.Bar(selector, dataUsersChart, optionsUsersChart, responsiveOptions);
            var drawing =
                UsersChart.on('draw', function (data) {
                    // If the draw event was triggered from drawing a point on the line chart
                    if (data.type === 'label' && data.axis === 'x') {

                        // We just offset the label X position to be in the middle between the current and next axis grid
                        data.element.attr({
                            dx: data.x + data.space / 2
                        });
                    }
                    var barHorizontalCenter, barVerticalCenter, label, value;
                    if (data.type === "bar") {
                        barHorizontalCenter = data.x1 + (data.element.width() * .5);
                        barVerticalCenter = data.y1 + (data.element.height() * -1) - 10;
                        value = data.element.attr('ct:value');
                        if (value !== '0') {
                            label = new Chartist.Svg('text');
                            label.text(value);
                            label.addClass("ct-barlabel");
                            label.addClass("ct-horizontal");
                            label.addClass("ct-end");
                            label.attr({
                                x: barHorizontalCenter,
                                y: barVerticalCenter,
                                'text-anchor': 'middle'
                            });
                            return data.group.append(label);
                        }
                    }

                });
            UsersChart.on("created", function () {
                $(UsersChart.container).parent().find("div > #spinner").hide()
            });

            //start animation for the Emails Subscription Chart
            // md.startAnimationForBarChart(UsersAlltimeChart);
            return UsersChart
        }
    },
    startAnimationForBarChart: function (chart) {

        chart.on('draw', function (data) {
            if (data.type === 'bar') {
                data.element.animate({
                    opacity: {
                        begin: 0,
                        dur: 500,
                        from: 0,
                        to: 1,
                        easing: 'ease'
                    }
                });
            }
        });

    },
    initFullCalendar: function () {
        $calendar = $('#fullCalendar');

        today = new Date();
        y = today.getFullYear();
        m = today.getMonth();
        d = today.getDate();

        $calendar.fullCalendar({
            viewRender: function (view, element) {
                // We make sure that we activate the perfect scrollbar when the view isn't on Month
                if (view.name != 'month') {
                    $(element).find('.fc-scroller').perfectScrollbar();
                }
            },
            header: {
                left: 'title',
                center: 'month,agendaWeek,agendaDay',
                right: 'prev,next,today'
            },
            defaultDate: today,
            selectable: true,
            selectHelper: true,
            views: {
                month: { // name of view
                    titleFormat: 'MMMM YYYY'
                    // other view-specific options here
                },
                week: {
                    titleFormat: " MMMM D YYYY"
                },
                day: {
                    titleFormat: 'D MMM, YYYY'
                }
            },

            select: function (start, end) {

                // on select we show the Sweet Alert modal with an input
                swal({
                    title: 'Create an Event',
                    html: '<div class="form-group">' +
                        '<input class="form-control" placeholder="Event Title" id="input-field">' +
                        '</div>',
                    showCancelButton: true,
                    confirmButtonClass: 'btn btn-success',
                    cancelButtonClass: 'btn btn-danger',
                    buttonsStyling: false
                }).then(function (result) {

                    var eventData;
                    event_title = $('#input-field').val();

                    if (event_title) {
                        eventData = {
                            title: event_title,
                            start: start,
                            end: end
                        };
                        $calendar.fullCalendar('renderEvent', eventData, true); // stick? = true
                    }

                    $calendar.fullCalendar('unselect');

                })
                    .catch(swal.noop);
            },
            editable: true,
            eventLimit: true, // allow "more" link when too many events


            // color classes: [ event-blue | event-azure | event-green | event-orange | event-red ]
            events: [{
                title: 'All Day Event',
                start: new Date(y, m, 1),
                className: 'event-default'
            },
                {
                    id: 999,
                    title: 'Repeating Event',
                    start: new Date(y, m, d - 4, 6, 0),
                    allDay: false,
                    className: 'event-rose'
                },
                {
                    id: 999,
                    title: 'Repeating Event',
                    start: new Date(y, m, d + 3, 6, 0),
                    allDay: false,
                    className: 'event-rose'
                },
                {
                    title: 'Meeting',
                    start: new Date(y, m, d - 1, 10, 30),
                    allDay: false,
                    className: 'event-green'
                },
                {
                    title: 'Lunch',
                    start: new Date(y, m, d + 7, 12, 0),
                    end: new Date(y, m, d + 7, 14, 0),
                    allDay: false,
                    className: 'event-red'
                },
                {
                    title: 'Md-pro Launch',
                    start: new Date(y, m, d - 2, 12, 0),
                    allDay: true,
                    className: 'event-azure'
                },
                {
                    title: 'Birthday Party',
                    start: new Date(y, m, d + 1, 19, 0),
                    end: new Date(y, m, d + 1, 22, 30),
                    allDay: false,
                    className: 'event-azure'
                },
                {
                    title: 'Click for Creative Tim',
                    start: new Date(y, m, 21),
                    end: new Date(y, m, 22),
                    url: 'http://www.creative-tim.com/',
                    className: 'event-orange'
                },
                {
                    title: 'Click for Google',
                    start: new Date(y, m, 21),
                    end: new Date(y, m, 22),
                    url: 'http://www.creative-tim.com/',
                    className: 'event-orange'
                }
            ]
        });
    },
};