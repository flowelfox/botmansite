function notification(type, message) {
    types = ['', 'info', 'danger', 'success', 'warning'];
    let icon = 'info';
    if (type === 'warning') {
        icon = 'warning'
    } else if (type === 'danger') {
        icon = 'error'
    } else if (type === 'success') {
        icon = 'notifications'
    }


    $.notify({
        icon: icon,
        message: message

    }, {
        type: type,
        timer: 3000,
        placement: {
            from: 'top',
            align: 'right'
        }
    });
}

function pad(num, size) {
    var s = num + "";
    while (s.length < size) s = "0" + s;
    return s;
}

function secondsTimeSpanToHMS(s) {
    var d = Math.floor(s/86400); //Get whole hours
    s -= d*86400;
    var h = Math.floor(s/3600); //Get whole hours
    s -= h*3600;
    var m = Math.floor(s/60); //Get remaining minutes
    s -= m*60;
    return [d, pad(h, 2), pad(m, 2), pad(s, 2)]; //zero padding on minutes and seconds
}