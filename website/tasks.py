import datetime
from typing import List, Dict

from asgiref.sync import async_to_sync
from celery import shared_task
from channels.layers import get_channel_layer
from pybotman import BotmanApi

from BotmanSite.settings import CERT


@shared_task
def get_bot_status(user_id: int, domain: str, token: str) -> bool:
    bot_api = BotmanApi(domain, token, CERT)
    result = bot_api.get_bot_status()

    cl = get_channel_layer()
    async_to_sync(cl.group_send)(f"user-{user_id}-control", {'type': 'receive', 'service': 'bot', 'status': result})
    return result


@shared_task
def get_api_status(user_id: int, domain: str, token: str) -> bool:
    bot_api = BotmanApi(domain, token, CERT)
    result = bot_api.get_api_status()

    cl = get_channel_layer()
    async_to_sync(cl.group_send)(f"user-{user_id}-control", {'type': 'receive', 'service': 'api', 'status': result})
    return result


@shared_task
def start_bot(user_id: int, domain: str, token: str) -> bool:
    bot_api = BotmanApi(domain, token, CERT, timeout=30)
    result = bot_api.start_bot()

    if result:
        message = {'type': 'success', 'text': "Bot started"}
    else:
        message = {'type': 'warning', 'text': "Can't start bot"}

    cl = get_channel_layer()
    async_to_sync(cl.group_send)(f"user-{user_id}-notification", {'type': 'receive', 'notification': message})
    async_to_sync(cl.group_send)(f"user-{user_id}-control", {'type': 'receive', 'service': 'bot', 'status': result})
    return result


@shared_task
def stop_bot(user_id: int, domain: str, token: str) -> bool:
    bot_api = BotmanApi(domain, token, CERT, timeout=30)
    result = bot_api.stop_bot()

    if result:
        message = {'type': 'success', 'text': "Bot stopped"}
    else:
        message = {'type': 'warning', 'text': "Can't stop bot"}

    cl = get_channel_layer()
    async_to_sync(cl.group_send)(f"user-{user_id}-notification", {'type': 'receive', 'notification': message})
    async_to_sync(cl.group_send)(f"user-{user_id}-control", {'type': 'receive', 'service': 'bot', 'status': not result})
    return result


@shared_task
def restart_bot(user_id: int, domain: str, token: str) -> bool:
    bot_api = BotmanApi(domain, token, CERT, timeout=60)
    result = bot_api.restart_bot()

    if result:
        message = {'type': 'info', 'text': "Bot restarted"}
    else:
        message = {'type': 'warning', 'text': "Can't restart bot"}

    cl = get_channel_layer()
    async_to_sync(cl.group_send)(f"user-{user_id}-notification", {'type': 'receive', 'notification': message})
    return result


@shared_task
def get_data(user_id: int, domain: str, token: str, table: str, columns: List[str] = None, limit: int = 300, offset: int = 0, ordering: int = 1) -> List[Dict]:
    bot_api = BotmanApi(domain, token, CERT)
    table_data = bot_api.get_data(table, columns, limit=limit, offset=offset, ordering=ordering)

    cl = get_channel_layer()

    async_to_sync(cl.group_send)(f"user-{user_id}-data", {'type': 'receive', 'table': table, 'columns': columns, 'data': table_data})
    return table_data


@shared_task
def count_data(user_id: int, domain: str, token: str, table: str) -> int:
    bot_api = BotmanApi(domain, token, CERT)
    quantity = bot_api.count_data(table)

    cl = get_channel_layer()

    async_to_sync(cl.group_send)(f"user-{user_id}-data", {'type': 'receive', 'table': table, 'quantity': quantity})
    return quantity


@shared_task
def bot_distribution(user_id: int, domain: str, token: str, text: str, when: datetime.datetime, web_preview: bool, notification: bool) -> bool:
    bot_api = BotmanApi(domain, token, CERT)
    result = bot_api.distribution(text, when, web_preview, notification)

    if result:
        message = {'type': 'success', 'text': "Message was sent"}
    else:
        message = {'type': 'warning', 'text': "Message was not sent"}

    cl = get_channel_layer()
    async_to_sync(cl.group_send)(f"user-{user_id}-notification", {'type': 'receive', 'notification': message})
    return result


@shared_task
def get_users(user_id: int, domain: str, token: str, websocket: str, limit: int = 500, offset: int = 0, ordering: int = -1):
    bot_api = BotmanApi(domain, token, CERT)
    users = bot_api.get_users(limit=limit, offset=offset, ordering=ordering)

    cl = get_channel_layer()

    async_to_sync(cl.group_send)(f"user-{user_id}-{websocket}", {'type': 'receive', 'task': 'get_users', 'data': users})


@shared_task
def get_uptime(user_id: int, domain: str, token: str, websocket: str):
    bot_api = BotmanApi(domain, token, CERT)
    result = bot_api.uptime()
    uptime = result.total_seconds() if not isinstance(result, bool) else result

    cl = get_channel_layer()
    async_to_sync(cl.group_send)(f"user-{user_id}-{websocket}", {'type': 'receive', 'task': 'get_uptime', 'data': uptime})


@shared_task
def get_start_date(user_id: int, domain: str, token: str, websocket: str):
    bot_api = BotmanApi(domain, token, CERT)
    start_date = bot_api.start_date()

    cl = get_channel_layer()
    async_to_sync(cl.group_send)(f"user-{user_id}-{websocket}", {'type': 'receive', 'task': 'get_start_date', 'data': start_date.isoformat() if start_date else False})


@shared_task
def get_stats(user_id: int, domain: str, token: str, websocket: str):
    bot_api = BotmanApi(domain, token, CERT)
    result = bot_api.get_stats()

    cl = get_channel_layer()
    async_to_sync(cl.group_send)(f"user-{user_id}-{websocket}", {'type': 'receive', 'task': 'get_stats', 'data': result})
