��    K      t  e   �      `  
   a     l     z     �     �     �     �  
   �     �     �  	        "  !   '     I     g  4   �  "   �     �     �     �  	         
               (  #   0  )   T     ~  $   �  *   �     �  
   �  
   	     	     	      	     =	  %   R	     x	     ~	  E   �	     �	     �	     �	     �	     
     	
     (
     8
     I
     a
     w
     �
     �
  /   �
  .   �
     ,     4     ;  %   @     f      �     �     �     �     �     �     �  7        @     E     b     f     m  �  t     @     Q     l     ~     �     �  %   �     �     �       !        :     C     X     k     ~     �     �  (   �     �  
   �            )        G  ^   \  p   �     ,     1     B  '   ]  +   �     �  
   �     �  
   �     �     �  
                  :  !   Q     s  =   �     �     �  *   �  %        3  
   @     K     R     a     r     ~     �     �     �  
   �     �     �     �  >   	  5   H  7   ~  9   �  @   �  %   1     W     d     s     x     �            1          &   +   :           ?      *   =   D   C      )       5           7       #   ;      .   @       2                               G             9         !   B      ,   H   8       -   A   $                     '          3         
      F           I   6   <      E                >           4   (   0          /             K       "   J   	   %       API Status Administrator All time All-time Api current statusRunning Api current statusStopped Average for all-time Bot Status Bot current statusRunning Bot current statusStopped Bot users Bots Button for restarting botRestart Button for starting botStart Button for stopping botStop Button which submits new profile dataUpdate Profile Came from in users tableCame from Chats Complete your profile Control Dashboard Day Distribution Edit Profile English Enter message and press Send button Here is users that registered in your bot ID of user in users talbeID Is user active in users tableActive Joined date and time in users tableJoined Last restart Last users Loading... Log out Login Login button on login pageLogin Menu sectionGeneral Message text for distributionMessage Month No Notification sound switch for distribution messageNotification sound Notifications Number of sessions Profile Registered users Russian Send distribution messageSend Session actions Session duration Site user avatarAvatar Site user emailEmail Site user first nameFist name Site user last nameLast name Site user loginUsername Telegram chat id of user in users talbeChat ID Telegram user language in users tableLanguage Unnamed Uptime User User login placeholder in fieldLogin User name in users tableName Username in users tableUsername Users Users chart all time Users chart day Users chart month Users chart week Users that block bot Web preview switch for distribution messageWeb preview Week When bot was startedStarted Yes avatar minute Project-Id-Version: 
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2019-10-24 15:04+0300
Last-Translator: Flowelcat <Flowelcat@gmail.com>
Language-Team: 
Language: ru
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || (n%100>=11 && n%100<=14)? 2 : 3);
X-Generator: Poedit 2.0.6
 Статус API Администратор Все время Все время Запущено Остановлено Среднее за всё время Статус бота Запущен Остановлен Пользователи бота Боты Перезапуск Запустить Выключить Обновить профиль Приглашен Чаты Заполните ваш профиль Управление Доска День Рассылка Редактировать профиль Английский Введите текст сообщения и нажмите кнопку Отправить Здесь показаны пользователи которые зарегистрированы в боте ИД Активный Присоединился Последний перезапуск Последние пользователи Загрузка... Выйти Вход Войти Основное Сообщение Месяц Нет Звук уведомления Уведомления Количество сессий Профиль Зарегистрированные пользователи Русский Отправить Действия внутри сессии Длительность сессии Аватар Имейл Имя Фамилия Юзернейм Чат ИД Язык Неназваный Время работы Пользователь Логин Имя Юзернейм Пользователи График пользователей за всё время График пользователей за день График пользователей за месяц График пользователей за неделю Пользователи заблокировавшие бота Предпросмотр ссылок Неделя Запущен Да аватар минута 